

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import spark.primitives.Rect;
import flash.display.*;
import flash.html.*;
import flash.debugger.*;
import flash.printing.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import flash.filesystem.*;
import flash.xml.*;
import mx.filters.*;
import flash.system.*;
import flash.profiler.*;
import spark.primitives.BitmapImage;
import flash.html.script.*;
import flash.external.*;
import flash.net.*;
import flash.desktop.*;
import spark.components.Group;
import flash.utils.*;
import flash.text.*;
import views.*;
import flash.data.*;
import mx.binding.*;
import flash.media.*;
import flash.ui.*;
import mx.styles.*;
import flash.errors.*;

class BindableProperty
{
	/*
	 * generated bindable wrapper for property drawCase (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'drawCase' moved to '_827401580drawCase'
	 */

    [Bindable(event="propertyChange")]
    public function get drawCase():spark.components.Group
    {
        return this._827401580drawCase;
    }

    public function set drawCase(value:spark.components.Group):void
    {
    	var oldValue:Object = this._827401580drawCase;
        if (oldValue !== value)
        {
            this._827401580drawCase = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "drawCase", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO1 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO1' moved to '_2119744616letterO1'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO1():spark.primitives.BitmapImage
    {
        return this._2119744616letterO1;
    }

    public function set letterO1(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744616letterO1;
        if (oldValue !== value)
        {
            this._2119744616letterO1 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO1", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO2 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO2' moved to '_2119744617letterO2'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO2():spark.primitives.BitmapImage
    {
        return this._2119744617letterO2;
    }

    public function set letterO2(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744617letterO2;
        if (oldValue !== value)
        {
            this._2119744617letterO2 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO2", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO3 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO3' moved to '_2119744618letterO3'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO3():spark.primitives.BitmapImage
    {
        return this._2119744618letterO3;
    }

    public function set letterO3(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744618letterO3;
        if (oldValue !== value)
        {
            this._2119744618letterO3 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO3", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO4 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO4' moved to '_2119744619letterO4'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO4():spark.primitives.BitmapImage
    {
        return this._2119744619letterO4;
    }

    public function set letterO4(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744619letterO4;
        if (oldValue !== value)
        {
            this._2119744619letterO4 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO4", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO5 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO5' moved to '_2119744620letterO5'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO5():spark.primitives.BitmapImage
    {
        return this._2119744620letterO5;
    }

    public function set letterO5(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744620letterO5;
        if (oldValue !== value)
        {
            this._2119744620letterO5 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO5", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO6 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO6' moved to '_2119744621letterO6'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO6():spark.primitives.BitmapImage
    {
        return this._2119744621letterO6;
    }

    public function set letterO6(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744621letterO6;
        if (oldValue !== value)
        {
            this._2119744621letterO6 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO6", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO7 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO7' moved to '_2119744622letterO7'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO7():spark.primitives.BitmapImage
    {
        return this._2119744622letterO7;
    }

    public function set letterO7(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744622letterO7;
        if (oldValue !== value)
        {
            this._2119744622letterO7 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO7", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO8 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO8' moved to '_2119744623letterO8'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO8():spark.primitives.BitmapImage
    {
        return this._2119744623letterO8;
    }

    public function set letterO8(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744623letterO8;
        if (oldValue !== value)
        {
            this._2119744623letterO8 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO8", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO9 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterO9' moved to '_2119744624letterO9'
	 */

    [Bindable(event="propertyChange")]
    public function get letterO9():spark.primitives.BitmapImage
    {
        return this._2119744624letterO9;
    }

    public function set letterO9(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744624letterO9;
        if (oldValue !== value)
        {
            this._2119744624letterO9 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO9", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX1 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX1' moved to '_2119744895letterX1'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX1():spark.primitives.BitmapImage
    {
        return this._2119744895letterX1;
    }

    public function set letterX1(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744895letterX1;
        if (oldValue !== value)
        {
            this._2119744895letterX1 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX1", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX2 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX2' moved to '_2119744896letterX2'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX2():spark.primitives.BitmapImage
    {
        return this._2119744896letterX2;
    }

    public function set letterX2(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744896letterX2;
        if (oldValue !== value)
        {
            this._2119744896letterX2 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX2", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX3 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX3' moved to '_2119744897letterX3'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX3():spark.primitives.BitmapImage
    {
        return this._2119744897letterX3;
    }

    public function set letterX3(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744897letterX3;
        if (oldValue !== value)
        {
            this._2119744897letterX3 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX3", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX4 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX4' moved to '_2119744898letterX4'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX4():spark.primitives.BitmapImage
    {
        return this._2119744898letterX4;
    }

    public function set letterX4(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744898letterX4;
        if (oldValue !== value)
        {
            this._2119744898letterX4 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX4", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX5 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX5' moved to '_2119744899letterX5'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX5():spark.primitives.BitmapImage
    {
        return this._2119744899letterX5;
    }

    public function set letterX5(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744899letterX5;
        if (oldValue !== value)
        {
            this._2119744899letterX5 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX5", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX6 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX6' moved to '_2119744900letterX6'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX6():spark.primitives.BitmapImage
    {
        return this._2119744900letterX6;
    }

    public function set letterX6(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744900letterX6;
        if (oldValue !== value)
        {
            this._2119744900letterX6 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX6", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX7 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX7' moved to '_2119744901letterX7'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX7():spark.primitives.BitmapImage
    {
        return this._2119744901letterX7;
    }

    public function set letterX7(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744901letterX7;
        if (oldValue !== value)
        {
            this._2119744901letterX7 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX7", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX8 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX8' moved to '_2119744902letterX8'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX8():spark.primitives.BitmapImage
    {
        return this._2119744902letterX8;
    }

    public function set letterX8(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744902letterX8;
        if (oldValue !== value)
        {
            this._2119744902letterX8 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX8", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX9 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'letterX9' moved to '_2119744903letterX9'
	 */

    [Bindable(event="propertyChange")]
    public function get letterX9():spark.primitives.BitmapImage
    {
        return this._2119744903letterX9;
    }

    public function set letterX9(value:spark.primitives.BitmapImage):void
    {
    	var oldValue:Object = this._2119744903letterX9;
        if (oldValue !== value)
        {
            this._2119744903letterX9 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX9", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property oWinCase (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'oWinCase' moved to '_1103398467oWinCase'
	 */

    [Bindable(event="propertyChange")]
    public function get oWinCase():spark.components.Group
    {
        return this._1103398467oWinCase;
    }

    public function set oWinCase(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1103398467oWinCase;
        if (oldValue !== value)
        {
            this._1103398467oWinCase = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "oWinCase", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont1 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont1' moved to '_1093630907rectCont1'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont1():spark.components.Group
    {
        return this._1093630907rectCont1;
    }

    public function set rectCont1(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630907rectCont1;
        if (oldValue !== value)
        {
            this._1093630907rectCont1 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont1", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont2 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont2' moved to '_1093630908rectCont2'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont2():spark.components.Group
    {
        return this._1093630908rectCont2;
    }

    public function set rectCont2(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630908rectCont2;
        if (oldValue !== value)
        {
            this._1093630908rectCont2 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont2", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont3 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont3' moved to '_1093630909rectCont3'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont3():spark.components.Group
    {
        return this._1093630909rectCont3;
    }

    public function set rectCont3(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630909rectCont3;
        if (oldValue !== value)
        {
            this._1093630909rectCont3 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont3", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont4 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont4' moved to '_1093630910rectCont4'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont4():spark.components.Group
    {
        return this._1093630910rectCont4;
    }

    public function set rectCont4(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630910rectCont4;
        if (oldValue !== value)
        {
            this._1093630910rectCont4 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont4", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont5 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont5' moved to '_1093630911rectCont5'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont5():spark.components.Group
    {
        return this._1093630911rectCont5;
    }

    public function set rectCont5(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630911rectCont5;
        if (oldValue !== value)
        {
            this._1093630911rectCont5 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont5", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont6 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont6' moved to '_1093630912rectCont6'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont6():spark.components.Group
    {
        return this._1093630912rectCont6;
    }

    public function set rectCont6(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630912rectCont6;
        if (oldValue !== value)
        {
            this._1093630912rectCont6 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont6", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont7 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont7' moved to '_1093630913rectCont7'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont7():spark.components.Group
    {
        return this._1093630913rectCont7;
    }

    public function set rectCont7(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630913rectCont7;
        if (oldValue !== value)
        {
            this._1093630913rectCont7 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont7", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont8 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont8' moved to '_1093630914rectCont8'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont8():spark.components.Group
    {
        return this._1093630914rectCont8;
    }

    public function set rectCont8(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630914rectCont8;
        if (oldValue !== value)
        {
            this._1093630914rectCont8 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont8", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property rectCont9 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'rectCont9' moved to '_1093630915rectCont9'
	 */

    [Bindable(event="propertyChange")]
    public function get rectCont9():spark.components.Group
    {
        return this._1093630915rectCont9;
    }

    public function set rectCont9(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1093630915rectCont9;
        if (oldValue !== value)
        {
            this._1093630915rectCont9 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "rectCont9", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect' moved to '_1322317864tempRect'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect():spark.primitives.Rect
    {
        return this._1322317864tempRect;
    }

    public function set tempRect(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1322317864tempRect;
        if (oldValue !== value)
        {
            this._1322317864tempRect = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect2 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect2' moved to '_1957819226tempRect2'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect2():spark.primitives.Rect
    {
        return this._1957819226tempRect2;
    }

    public function set tempRect2(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819226tempRect2;
        if (oldValue !== value)
        {
            this._1957819226tempRect2 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect2", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect3 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect3' moved to '_1957819227tempRect3'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect3():spark.primitives.Rect
    {
        return this._1957819227tempRect3;
    }

    public function set tempRect3(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819227tempRect3;
        if (oldValue !== value)
        {
            this._1957819227tempRect3 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect3", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect4 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect4' moved to '_1957819228tempRect4'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect4():spark.primitives.Rect
    {
        return this._1957819228tempRect4;
    }

    public function set tempRect4(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819228tempRect4;
        if (oldValue !== value)
        {
            this._1957819228tempRect4 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect4", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect5 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect5' moved to '_1957819229tempRect5'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect5():spark.primitives.Rect
    {
        return this._1957819229tempRect5;
    }

    public function set tempRect5(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819229tempRect5;
        if (oldValue !== value)
        {
            this._1957819229tempRect5 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect5", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect6 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect6' moved to '_1957819230tempRect6'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect6():spark.primitives.Rect
    {
        return this._1957819230tempRect6;
    }

    public function set tempRect6(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819230tempRect6;
        if (oldValue !== value)
        {
            this._1957819230tempRect6 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect6", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect7 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect7' moved to '_1957819231tempRect7'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect7():spark.primitives.Rect
    {
        return this._1957819231tempRect7;
    }

    public function set tempRect7(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819231tempRect7;
        if (oldValue !== value)
        {
            this._1957819231tempRect7 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect7", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect8 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect8' moved to '_1957819232tempRect8'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect8():spark.primitives.Rect
    {
        return this._1957819232tempRect8;
    }

    public function set tempRect8(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819232tempRect8;
        if (oldValue !== value)
        {
            this._1957819232tempRect8 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect8", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property tempRect9 (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'tempRect9' moved to '_1957819233tempRect9'
	 */

    [Bindable(event="propertyChange")]
    public function get tempRect9():spark.primitives.Rect
    {
        return this._1957819233tempRect9;
    }

    public function set tempRect9(value:spark.primitives.Rect):void
    {
    	var oldValue:Object = this._1957819233tempRect9;
        if (oldValue !== value)
        {
            this._1957819233tempRect9 = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tempRect9", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property xWinCase (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'xWinCase' moved to '_1696992660xWinCase'
	 */

    [Bindable(event="propertyChange")]
    public function get xWinCase():spark.components.Group
    {
        return this._1696992660xWinCase;
    }

    public function set xWinCase(value:spark.components.Group):void
    {
    	var oldValue:Object = this._1696992660xWinCase;
        if (oldValue !== value)
        {
            this._1696992660xWinCase = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "xWinCase", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterO (protected)
	 * - generated setter
	 * - generated getter
	 * - original protected var 'letterO' moved to '_68378857letterO'
	 */

    [Bindable(event="propertyChange")]
    protected function get letterO():Bitmap
    {
        return this._68378857letterO;
    }

    protected function set letterO(value:Bitmap):void
    {
    	var oldValue:Object = this._68378857letterO;
        if (oldValue !== value)
        {
            this._68378857letterO = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterO", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property letterX (protected)
	 * - generated setter
	 * - generated getter
	 * - original protected var 'letterX' moved to '_68378866letterX'
	 */

    [Bindable(event="propertyChange")]
    protected function get letterX():Bitmap
    {
        return this._68378866letterX;
    }

    protected function set letterX(value:Bitmap):void
    {
    	var oldValue:Object = this._68378866letterX;
        if (oldValue !== value)
        {
            this._68378866letterX = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "letterX", oldValue, value));
        }
    }



}
