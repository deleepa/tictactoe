

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;
import mx.core.IPropertyChangeNotifier;
import mx.events.PropertyChangeEvent;
import mx.utils.ObjectProxy;
import mx.utils.UIDUtil;

import flash.display.*;
import flash.html.*;
import flash.debugger.*;
import flash.printing.*;
import flash.geom.*;
import flash.events.*;
import flash.accessibility.*;
import flash.filesystem.*;
import flash.xml.*;
import mx.filters.*;
import flash.system.*;
import flash.profiler.*;
import flash.html.script.*;
import flash.external.*;
import flash.net.*;
import spark.components.TextInput;
import flash.desktop.*;
import spark.components.Group;
import flash.utils.*;
import flash.text.*;
import views.*;
import spark.components.Label;
import flash.data.*;
import mx.binding.*;
import flash.media.*;
import flash.ui.*;
import mx.styles.*;
import flash.errors.*;

class BindableProperty
{
	/*
	 * generated bindable wrapper for property enterHighScore (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'enterHighScore' moved to '_374345192enterHighScore'
	 */

    [Bindable(event="propertyChange")]
    public function get enterHighScore():spark.components.Group
    {
        return this._374345192enterHighScore;
    }

    public function set enterHighScore(value:spark.components.Group):void
    {
    	var oldValue:Object = this._374345192enterHighScore;
        if (oldValue !== value)
        {
            this._374345192enterHighScore = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "enterHighScore", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property highscoreList (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'highscoreList' moved to '_1311563026highscoreList'
	 */

    [Bindable(event="propertyChange")]
    public function get highscoreList():spark.components.Label
    {
        return this._1311563026highscoreList;
    }

    public function set highscoreList(value:spark.components.Label):void
    {
    	var oldValue:Object = this._1311563026highscoreList;
        if (oldValue !== value)
        {
            this._1311563026highscoreList = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "highscoreList", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property scoreEntered (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'scoreEntered' moved to '_71569787scoreEntered'
	 */

    [Bindable(event="propertyChange")]
    public function get scoreEntered():spark.components.Group
    {
        return this._71569787scoreEntered;
    }

    public function set scoreEntered(value:spark.components.Group):void
    {
    	var oldValue:Object = this._71569787scoreEntered;
        if (oldValue !== value)
        {
            this._71569787scoreEntered = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "scoreEntered", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property userName (public)
	 * - generated setter
	 * - generated getter
	 * - original public var 'userName' moved to '_266666762userName'
	 */

    [Bindable(event="propertyChange")]
    public function get userName():spark.components.TextInput
    {
        return this._266666762userName;
    }

    public function set userName(value:spark.components.TextInput):void
    {
    	var oldValue:Object = this._266666762userName;
        if (oldValue !== value)
        {
            this._266666762userName = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "userName", oldValue, value));
        }
    }

	/*
	 * generated bindable wrapper for property scoreText (protected)
	 * - generated setter
	 * - generated getter
	 * - original protected var 'scoreText' moved to '_2128961247scoreText'
	 */

    [Bindable(event="propertyChange")]
    protected function get scoreText():String
    {
        return this._2128961247scoreText;
    }

    protected function set scoreText(value:String):void
    {
    	var oldValue:Object = this._2128961247scoreText;
        if (oldValue !== value)
        {
            this._2128961247scoreText = value;
           if (this.hasEventListener("propertyChange"))
               this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "scoreText", oldValue, value));
        }
    }



}
