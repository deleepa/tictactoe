






package
{
import mx.core.IFlexModuleFactory;
import mx.binding.ArrayElementWatcher;
import mx.binding.FunctionReturnWatcher;
import mx.binding.IWatcherSetupUtil2;
import mx.binding.PropertyWatcher;
import mx.binding.RepeaterComponentWatcher;
import mx.binding.RepeaterItemWatcher;
import mx.binding.StaticPropertyWatcher;
import mx.binding.XMLWatcher;
import mx.binding.Watcher;

[ExcludeClass]
public class _views_GameWatcherSetupUtil
    implements mx.binding.IWatcherSetupUtil2
{
    public function _views_GameWatcherSetupUtil()
    {
        super();
    }

    public static function init(fbs:IFlexModuleFactory):void
    {
        import views.Game;
        (views.Game).watcherSetupUtil = new _views_GameWatcherSetupUtil();
    }

    public function setup(target:Object,
                          propertyGetter:Function,
                          staticPropertyGetter:Function,
                          bindings:Array,
                          watchers:Array):void
    {
        import spark.primitives.Rect;
        import spark.components.TileGroup;
        import spark.primitives.Line;
        import mx.core.DeferredInstanceFromClass;
        import __AS3__.vec.Vector;
        import spark.components.View;
        import mx.binding.IBindingClient;
        import flash.events.MouseEvent;
        import mx.core.ClassFactory;
        import mx.core.IFactory;
        import mx.core.DeferredInstanceFromFunction;
        import flash.events.EventDispatcher;
        import spark.components.Button;
        import mx.core.IFlexModuleFactory;
        import spark.primitives.BitmapImage;
        import mx.binding.BindingManager;
        import mx.graphics.SolidColor;
        import mx.core.IDeferredInstance;
        import spark.components.Group;
        import mx.core.IPropertyChangeNotifier;
        import flash.events.IEventDispatcher;
        import spark.components.Label;
        import mx.graphics.SolidColorStroke;
        import mx.core.mx_internal;
        import mx.events.FlexEvent;
        import spark.components.VGroup;
        import flash.events.Event;

        // writeWatcher id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[1] = new mx.binding.PropertyWatcher("height",
                                                                             {
                heightChanged: true
            }
,
                                                                         // writeWatcherListeners id=1 size=1
        [
        bindings[2]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=18 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[18] = new mx.binding.PropertyWatcher("rectCont4",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=18 size=2
        [
        bindings[29],
        bindings[30]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=20 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[20] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=20 size=1
        [
        bindings[30]
        ]
,
                                                                 null
);

        // writeWatcher id=19 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[19] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=19 size=1
        [
        bindings[29]
        ]
,
                                                                 null
);

        // writeWatcher id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[2] = new mx.binding.PropertyWatcher("letterO",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=2 size=9
        [
        bindings[3],
        bindings[5],
        bindings[7],
        bindings[9],
        bindings[11],
        bindings[13],
        bindings[15],
        bindings[17],
        bindings[19]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=4 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[4] = new mx.binding.PropertyWatcher("rectCont2",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=4 size=2
        [
        bindings[21],
        bindings[22]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=6 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[6] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=6 size=1
        [
        bindings[22]
        ]
,
                                                                 null
);

        // writeWatcher id=5 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[5] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=5 size=1
        [
        bindings[21]
        ]
,
                                                                 null
);

        // writeWatcher id=11 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[11] = new mx.binding.PropertyWatcher("rectCont3",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=11 size=2
        [
        bindings[25],
        bindings[26]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=13 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[13] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=13 size=1
        [
        bindings[26]
        ]
,
                                                                 null
);

        // writeWatcher id=12 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[12] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=12 size=1
        [
        bindings[25]
        ]
,
                                                                 null
);

        // writeWatcher id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[0] = new mx.binding.PropertyWatcher("width",
                                                                             {
                widthChanged: true
            }
,
                                                                         // writeWatcherListeners id=0 size=3
        [
        bindings[0],
        bindings[1],
        bindings[2]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=25 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[25] = new mx.binding.PropertyWatcher("rectCont7",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=25 size=2
        [
        bindings[33],
        bindings[34]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=27 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[27] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=27 size=1
        [
        bindings[34]
        ]
,
                                                                 null
);

        // writeWatcher id=26 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[26] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=26 size=1
        [
        bindings[33]
        ]
,
                                                                 null
);

        // writeWatcher id=21 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[21] = new mx.binding.PropertyWatcher("rectCont6",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=21 size=2
        [
        bindings[31],
        bindings[32]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=23 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[23] = new mx.binding.PropertyWatcher("width",
                                                                             {
                widthChanged: true
            }
,
                                                                         // writeWatcherListeners id=23 size=1
        [
        bindings[31]
        ]
,
                                                                 null
);

        // writeWatcher id=24 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[24] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=24 size=1
        [
        bindings[32]
        ]
,
                                                                 null
);

        // writeWatcher id=22 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[22] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=22 size=1
        [
        bindings[31]
        ]
,
                                                                 null
);

        // writeWatcher id=14 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[14] = new mx.binding.PropertyWatcher("rectCont9",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=14 size=4
        [
        bindings[27],
        bindings[28],
        bindings[35],
        bindings[36]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=17 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[17] = new mx.binding.PropertyWatcher("height",
                                                                             {
                heightChanged: true
            }
,
                                                                         // writeWatcherListeners id=17 size=1
        [
        bindings[28]
        ]
,
                                                                 null
);

        // writeWatcher id=28 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[28] = new mx.binding.PropertyWatcher("width",
                                                                             {
                widthChanged: true
            }
,
                                                                         // writeWatcherListeners id=28 size=1
        [
        bindings[35]
        ]
,
                                                                 null
);

        // writeWatcher id=16 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[16] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=16 size=2
        [
        bindings[28],
        bindings[36]
        ]
,
                                                                 null
);

        // writeWatcher id=15 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[15] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=15 size=2
        [
        bindings[27],
        bindings[35]
        ]
,
                                                                 null
);

        // writeWatcher id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[3] = new mx.binding.PropertyWatcher("letterX",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=3 size=9
        [
        bindings[4],
        bindings[6],
        bindings[8],
        bindings[10],
        bindings[12],
        bindings[14],
        bindings[16],
        bindings[18],
        bindings[20]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=7 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[7] = new mx.binding.PropertyWatcher("rectCont8",
                                                                             {
                propertyChange: true
            }
,
                                                                         // writeWatcherListeners id=7 size=2
        [
        bindings[23],
        bindings[24]
        ]
,
                                                                 propertyGetter
);

        // writeWatcher id=10 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[10] = new mx.binding.PropertyWatcher("height",
                                                                             {
                heightChanged: true
            }
,
                                                                         // writeWatcherListeners id=10 size=1
        [
        bindings[24]
        ]
,
                                                                 null
);

        // writeWatcher id=9 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[9] = new mx.binding.PropertyWatcher("y",
                                                                             {
                yChanged: true
            }
,
                                                                         // writeWatcherListeners id=9 size=1
        [
        bindings[24]
        ]
,
                                                                 null
);

        // writeWatcher id=8 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher shouldWriteChildren=true
        watchers[8] = new mx.binding.PropertyWatcher("x",
                                                                             {
                xChanged: true
            }
,
                                                                         // writeWatcherListeners id=8 size=1
        [
        bindings[23]
        ]
,
                                                                 null
);


        // writeWatcherBottom id=1 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[1].updateParent(target);

 





        // writeWatcherBottom id=18 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[18].updateParent(target);

 





        // writeWatcherBottom id=20 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[18].addChild(watchers[20]);

 





        // writeWatcherBottom id=19 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[18].addChild(watchers[19]);

 





        // writeWatcherBottom id=2 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[2].updateParent(target);

 





        // writeWatcherBottom id=4 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[4].updateParent(target);

 





        // writeWatcherBottom id=6 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[4].addChild(watchers[6]);

 





        // writeWatcherBottom id=5 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[4].addChild(watchers[5]);

 





        // writeWatcherBottom id=11 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[11].updateParent(target);

 





        // writeWatcherBottom id=13 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[11].addChild(watchers[13]);

 





        // writeWatcherBottom id=12 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[11].addChild(watchers[12]);

 





        // writeWatcherBottom id=0 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[0].updateParent(target);

 





        // writeWatcherBottom id=25 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[25].updateParent(target);

 





        // writeWatcherBottom id=27 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[25].addChild(watchers[27]);

 





        // writeWatcherBottom id=26 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[25].addChild(watchers[26]);

 





        // writeWatcherBottom id=21 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[21].updateParent(target);

 





        // writeWatcherBottom id=23 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[21].addChild(watchers[23]);

 





        // writeWatcherBottom id=24 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[21].addChild(watchers[24]);

 





        // writeWatcherBottom id=22 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[21].addChild(watchers[22]);

 





        // writeWatcherBottom id=14 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[14].updateParent(target);

 





        // writeWatcherBottom id=17 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[14].addChild(watchers[17]);

 





        // writeWatcherBottom id=28 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[14].addChild(watchers[28]);

 





        // writeWatcherBottom id=16 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[14].addChild(watchers[16]);

 





        // writeWatcherBottom id=15 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[14].addChild(watchers[15]);

 





        // writeWatcherBottom id=3 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[3].updateParent(target);

 





        // writeWatcherBottom id=7 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[7].updateParent(target);

 





        // writeWatcherBottom id=10 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[7].addChild(watchers[10]);

 





        // writeWatcherBottom id=9 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[7].addChild(watchers[9]);

 





        // writeWatcherBottom id=8 shouldWriteSelf=true class=flex2.compiler.as3.binding.PropertyWatcher
        watchers[7].addChild(watchers[8]);

 





    }
}

}
