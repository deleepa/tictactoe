
package views
{
import flash.accessibility.*;
import flash.data.*;
import flash.debugger.*;
import flash.desktop.*;
import flash.display.*;
import flash.errors.*;
import flash.events.*;
import flash.external.*;
import flash.filesystem.*;
import flash.geom.*;
import flash.html.*;
import flash.html.script.*;
import flash.media.*;
import flash.net.*;
import flash.printing.*;
import flash.profiler.*;
import flash.system.*;
import flash.text.*;
import flash.ui.*;
import flash.utils.*;
import flash.xml.*;
import mx.binding.*;
import mx.core.ClassFactory;
import mx.core.DeferredInstanceFromClass;
import mx.core.DeferredInstanceFromFunction;
import mx.core.IDeferredInstance;
import mx.core.IFactory;
import mx.core.IFlexModuleFactory;
import mx.core.IPropertyChangeNotifier;
import mx.core.mx_internal;
import mx.filters.*;
import mx.styles.*;
import spark.components.Group;
import spark.components.View;
import spark.primitives.BitmapImage;
import spark.primitives.Rect;
import spark.primitives.Rect;
import spark.components.TileGroup;
import spark.components.Label;
import spark.primitives.Line;
import mx.graphics.SolidColorStroke;
import spark.components.VGroup;
import mx.graphics.SolidColor;
import spark.components.Group;
import spark.components.View;
import spark.components.Button;

public class Game extends spark.components.View
{
	public function Game() {}

	[Bindable]
	public var rectCont1 : spark.components.Group;
	[Bindable]
	public var tempRect : spark.primitives.Rect;
	[Bindable]
	public var letterO1 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX1 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont2 : spark.components.Group;
	[Bindable]
	public var tempRect2 : spark.primitives.Rect;
	[Bindable]
	public var letterO2 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX2 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont3 : spark.components.Group;
	[Bindable]
	public var tempRect3 : spark.primitives.Rect;
	[Bindable]
	public var letterO3 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX3 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont4 : spark.components.Group;
	[Bindable]
	public var tempRect4 : spark.primitives.Rect;
	[Bindable]
	public var letterO4 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX4 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont5 : spark.components.Group;
	[Bindable]
	public var tempRect5 : spark.primitives.Rect;
	[Bindable]
	public var letterO5 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX5 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont6 : spark.components.Group;
	[Bindable]
	public var tempRect6 : spark.primitives.Rect;
	[Bindable]
	public var letterO6 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX6 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont7 : spark.components.Group;
	[Bindable]
	public var tempRect7 : spark.primitives.Rect;
	[Bindable]
	public var letterO7 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX7 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont8 : spark.components.Group;
	[Bindable]
	public var tempRect8 : spark.primitives.Rect;
	[Bindable]
	public var letterO8 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX8 : spark.primitives.BitmapImage;
	[Bindable]
	public var rectCont9 : spark.components.Group;
	[Bindable]
	public var tempRect9 : spark.primitives.Rect;
	[Bindable]
	public var letterO9 : spark.primitives.BitmapImage;
	[Bindable]
	public var letterX9 : spark.primitives.BitmapImage;
	[Bindable]
	public var xWinCase : spark.components.Group;
	[Bindable]
	public var oWinCase : spark.components.Group;
	[Bindable]
	public var drawCase : spark.components.Group;

	mx_internal var _bindings : Array;
	mx_internal var _watchers : Array;
	mx_internal var _bindingsByDestination : Object;
	mx_internal var _bindingsBeginWithWord : Object;

include "C:/Users/Deleepa/Adobe Flash Builder 4.6/FlexTicTacToe/src/views/Game.mxml:7,302";

}}
